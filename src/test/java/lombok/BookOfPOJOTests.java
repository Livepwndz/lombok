/**
 * 
 */
package lombok;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Kigozi
 *
 */
public class BookOfPOJOTests {
	@Test
	public void shouldCreatePOJO() throws Exception {
		BookOfLombok pojo = new BookOfLombok( "Refined Contextual Programming", "Kigozi Mz'bu", new Date() );
		Assert.assertNotNull( pojo );
	}
}
