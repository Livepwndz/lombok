/**
 * 
 */
package lombok;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Kigozi
 *
 */
public class BookOfLombokTests {
	@Test
	public void shouldCreatePOJO() throws Exception {
		BookOfPOJO pojo = new BookOfPOJO( "Contextual Programming", "Kigozi Mz'bu", new Date() );
		Assert.assertNotNull( pojo );
	}
}
