/**
 * 
 */
package lombok;

import java.util.Date;

/**
 * @author Kigozi
 *
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookOfLombok {
	
	private String title;
	private String author;
	private Date dateOfPublication;
	
}
